CREATE TABLE physical_data.contact_details (
    trainees_id    BigSERIAL PRIMARY KEY,
    address        VARCHAR(150),
    city           VARCHAR(50),
    state VARCHAR(50),
    country VARCHAR(50),
    postal_code VARCHAR(20),
    created_date TIMESTAMP DEFAULT current_timestamp
);

ALTER TABLE physical_data.contact_details
ADD COLUMN record_ts DATE DEFAULT current_date;

SELECT contact_details.*, record_ts
FROM physical_data.contact_details;


